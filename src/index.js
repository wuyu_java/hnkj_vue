import _ from "lodash"
import "./index.css"
import Vue from 'vue'
// import index from './index.vue'
import button from './pages/button.vue'
import VueRouter from 'vue-router'
import home from './pages/home.vue'
import about from './pages/about.vue'
import test from './pages/test.vue'  

//路由 : 根据浏览器地址展示对应的vue组件(页面)(插件)
//vue提供了插件机制,你可以为vue添加属性,方法,实例等..
//注册插件
Vue.use(VueRouter)
//定义路由规则
const Routers=[
  {
    path: '/home',
    meta: {
      title: '首页'
    },
    component: home
  },
  {
    path: '/about',
    meta: {
      title: '介绍'
    },
    component: about
  },{
    path: '*',
    redirect: '/home'
  }
]

//设置路由配置
const RouterConfig = {
  mode: 'history',
  routes: Routers
}

const router = new VueRouter(RouterConfig);

// router.beforeEach(function(to,from,next){

// })

//当跳转路由显示之前执行
/**
 * to : 即将进入的目标路由对象
 * from : 当前导航即将离开的路由对象
 * next : 调用该方法后,才能进入下一个操作
 */
router.beforeEach((to, from, next)=>{
  window.document.title=to.meta.title;
  next();
})
// router.afterEach((to, from, next)=>{
//   window.scrollTo(0,100)
// });



//render 可以将一个vue组件加载到指定的位置(template)
let app =new Vue({
  el: "#app",
  router: router,
  data: {
    msg: "hello vue app"
  },
  // render: function (createElement){
  //   return new createElement(index)
  // }
  render: h => h(test)
})

// function component() {
//     var element = document.createElement('div');
  
//     // Lodash（目前通过一个 script 脚本引入）对于执行这一行是必需的
//     element.innerHTML = _.join(['Hello', '{{msg}}'], ' ');
//     element.className = "demo";
//     element.id="app";

   

//     return element;
//   }
  
//   document.body.appendChild(component());
  
  